+++
title = "Giloop - des projets plein la tête"
description = "Présentation de mon parcours et de quelques projets"
outputs = ["Reveal"]
[logo]
src = "logo.png"
alt = "Giloop"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
# Régler auto_slide pour un diaporama automatique
# valeur en millisecondes, 0 pour défilement manuel
auto_slide = 0
loop = false
transition = "slide"
transition_speed = "fast"
+++

{{< slide transition="zoom" background-image="elephant.jpg" >}}

### <span style="color:#b1f">Gilles Gonon</span>

<span style="color:#FFF; font-weight:bold;">Fantaisies photographiques et autres histoires personnelles</span>

---

# 🧐 Présentation

- 👋 Parcours personnel
- 🎯 Projets industriels
- 📝 Projets photos

---

## 😷 Parcours personnel

<ul>
    <li class="fragment">Scolarité longue</li>
    <li class="fragment">Prépa → École d'ingé → DEA ACoustique</li>
    <li class="fragment">1998 : Thèse en traitement du signal 🤓</li>
    <li class="fragment">Rencontre du Professeur David Rousseau 🥰</li>
    <li class="fragment">2004 : Post-doc Rennes IRISA : biométrie et voix</li>
    <li class="fragment">2009 : Acsystème, la diversité des projets</li>
    <li class="fragment">2019 : Institut Solacroup → formation numérique atypique</li>
</ul>

<h6 class="fragment" style="color:#b1f;">→ Une certaine dérive à l'ouest</h6>

---

## 🤩 Parcours parallèle

<ul>
    <li class="fragment">1998 : 🎹 Stravinsky & Hutch Orchestra 🕺</li>
    <li class="fragment">2003 : Pérou – ONG Takiwasi, Réhabilitation et thérapies traditionnelles</li>
    <li class="fragment">2009 : Année musicale</li>
    <li class="fragment">2014 : Invention du Gueulomaton</li>
    <li class="fragment">2019 : Tinténiac En Transition</li>
</ul>

<h6 class="fragment" style="color:#b1f;">→ Qu’est -ce qui vous nourrit ?</h6>

---

#### Quelques projets chez [Acsystème](http://www.acsysteme.fr)

<ul>
    <li class="fragment">Tri de poisson par réseau de neurones (2009)</li>
    <li class="fragment">Analyse Lidar : géométrie 3D</li>
    <li class="fragment">Calcul de fond propres, modèles probabilistes (finance)</li>
    <li class="fragment">Optimisation mécanique : (Airbus)</li>
    <li class="fragment">Algorithme d'empilage de colis 3D (logistique)</li>
    <li class="fragment">Traitement du signal : usure du pneu et reconstruction trajectoire (Michelin)</li>
    <li class="fragment">Machine Learning : améliorer le diagnostic valise</li>
</ul>

<h6 class="fragment" style="color:#b1f;">→ De l'expertise en entreprise</h6>

---

### L'institut Marie-Thérèse Solacroup

<ul>
    <li class="fragment">Formations numériques développeur Web</li>
    <li class="fragment">Remettre de l'humain dans tout ça</li>
    <li class="fragment">Un public atypique : où se situe la normalité ?</li>
    <li class="fragment">Le numérique nous sauvera-t'il ?</li>
</ul>

<h6 class="fragment" style="color:#b1f;">→ Et là c'est le doute !</h6>

---

##### Le [Gueulomaton](https://gueulomaton.org)

{{< figure src="Cabine-gueulo.jpg" width="60%">}}

---

##### Le Gueulomaton côté cabine

<ul>
    <li class="fragment">Un PC</li>
    <li class="fragment">Une application C++, <a href="https://www.openframeworks.cc/">openframeworks</a></li>
    <li class="fragment">Le son déclenche la photo</li>
    <li class="fragment">Le son est enregistré (buffer circulaire)</li>
    <li class="fragment">Photo & Son transmis à la borne</li>
</ul>


---

##### Le Gueulomaton côté borne

<ul>
    <li class="fragment">Un Raspberry Pi + Arduino</li>
    <li class="fragment">Post traitement (redimensionnement)</li>
    <li class="fragment">Un programme <a href="https://processing.org/">Processing</a> pour la galerie</li>
    <li class="fragment">Une imprimante pro DNP-DS620 (rouleaux de 400 photos)</li>
</ul>

<h6 class="fragment" style="color:#b1f;">→ Open source</h6>

---

### Des milliers de cris !

- Une vraie base de données son + image
- À exploiter ...

--- 

### Variations n°1 : Les bouilles à facettes

- Un [exemple de galerie](https://gueulomaton.org/BAF/)
- Une appli C++ : détection de 2 visages déclenchement de la photo
- Post traitement : 
  - calcul du morphing en Python
  - Génération d'un GIF

--- 

### Variations n°2 : Le Sautomaton

- Tribute to Philippe Halsman
- Prise d'une rafale de photos
- Post-traitement Python (openCV) pour sélection
- Un exemple de [galerie](https://gueulomaton.org/sautomaton/galerie.php?n=vortex17)
  
> "When you ask a person to jump, his attention is mostly directed toward the act of jumping and the mask falls so that the real person appears” ~ Philippe Halsman

--- 

### Variations n°3 : l'illuminaton

- Light-painting 
- Le temps d'une pause, l'esprit de la photo
- Pas de post-traitement
- Un exemple de [galerie](https://gueulomaton.org/illuminaton/galerie.php?n=gmr17)


--- 

### L'esprit des variations 

<ul>
    <li class="fragment">Saisir un moment inhabituel en photo</li>
    <li class="fragment">Automatiser le déclenchement</li>
    <li class="fragment">Automatiser le post-traitement</li>
    <li class="fragment">S'amuser</li>
    <li class="fragment">Sortir du cliché <em>Miroir miroir</em></li>
</ul>

<h6 class="fragment" style="color:#b1f;">Imprimer c'est gagné</h6>